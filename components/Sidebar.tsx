import { AiFillGithub, AiFillGitlab, AiFillLinkedin, AiFillYoutube, } from "react-icons/ai";
import { GiTie } from "react-icons/gi";
import { GoLocation } from "react-icons/go";
import { useTheme } from "next-themes";
import Image from "next/image";

const Sidebar = () => {
  const { theme, setTheme } = useTheme();

  const changeTheme = () => {
    setTheme(theme === "light" ? "dark" : "light");
  };

  return (
    <>
      <Image
        src="/images/274449222_1682450418774556_5270343997621703208_n.jpg"
        alt="avatar"
        className=" mx-auto border rounded-full "
        height="128px"
        width="128px"
        layout="intrinsic"
        quality="100"
      />
      <h3 className="my-4 text-3xl font-medium tracking-wider font-kaushan">
        <span className="text-pink-400 ">Kritsada</span> Suphapha
      </h3>
      <p className="px-2 py-1 my-3 bg-gray-200 rounded-full dark:bg-dark-200 dark:bg-black-500">
        Software Engineering
      </p>
      {/* Resume */}
      <a
        href="/assets/Resume.pdf"
        download="Sumit Dey Resume.pdf"
        className="flex items-center justify-center px-2 py-1 my-2 bg-gray-200 rounded-full cursor-pointer dark:bg-dark-200 dark:bg-black-500"
      >
        <GiTie className="w-6 h-6" />
        <span>Download Resume</span>
      </a>

      {/* Socials */}
        {/* https://line.me/ti/p/4SbS4Wawfa */}

      <div className="flex justify-around w-9/12 mx-auto my-5 text-pink-400 md:w-full ">
        {/* <a href="https://www.youtube.com/channel/UClW8d1f5m0QAE_Ig024EP6A">
          <AiFillYoutube className="w-8 h-8 cursor-pointer" />
        </a>
        <a href="https://www.linkedin.com/in/sumit-dey-4a04431a9/" >
          <AiFillLinkedin className="w-8 h-8 cursor-pointer" />
        </a> */}
        <a href="https://gitlab.com/blackpoi55">
          <AiFillGitlab className="w-8 h-8 cursor-pointer" />{" "}
        </a>
      </div>

      {/* Contacts */}
      <div
        className="py-4 my-5 bg-gray-200 dark:bg-dark-200 dark:bg-black-500"
        style={{ marginLeft: "-1rem", marginRight: "-1rem" }}
      >
        <div className="flex items-center justify-center">
          <GoLocation className="mx-2" /> <span>Krung Thep Maha Nakhon (Bangkok),Thailand </span>
        </div>
        <p className="my-2 "> Blackpoi55@gmail.com </p>
        <p className="my-2"> 061-9917727 </p>
      </div>

      {/* Email Button */}

      {/* <button
        className="w-8/12 px-5 py-2 text-white bg-black rounded-full cursor-pointer bg-gradient-to-r from-pink-400 to-blue-400 hover:scale-105 focus:outline-none"
        onClick={() => window.open("mailto:Blackpoi55@gmail.com")}
      >
        Email me
      </button> */}
      <button
        onClick={changeTheme}
        className="w-8/12 px-5 py-2 my-4 text-white bg-black rounded-full cursor-pointer bg-gradient-to-r from-pink-400 to-blue-400 focus:outline-none hover:scale-105 "
      >
        {/* //TODO remove bg black */}
        Toggle Theme
      </button>
    </>
  );
};

export default Sidebar;
