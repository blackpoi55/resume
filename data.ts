import { RiComputerLine } from "react-icons/ri";
import { FaServer } from "react-icons/fa";
import { AiOutlineAntDesign, AiOutlineApi } from "react-icons/ai";
import { MdDeveloperMode } from "react-icons/md";
import { IProject, Service, Skill } from "./types";

import { BsCircleFill } from "react-icons/bs";

export const services: Service[] = [
  {
    Icon: RiComputerLine,
    title: "Frontend Development",
    about:
      "I can build a beautiful and scalable website <b> HTML</b>,<b>CSS</b>,<b>Tailwind</b>,<b>Next.js</b>  and <b>React.js</b> ",
  },
  {
    Icon: FaServer,
    title: "Backend  Development",
    about:
      "I can handle database, server and api  <b>Express </b> & other popular frameworks",
  },
  {
    Icon: AiOutlineApi,
    title: "API Development",
    about:
      "I can develop robust  REST API using <b>Node API</b> ",
  },
  // {
  //   Icon: MdDeveloperMode,
  //   title: "Competitive Coder",
  //   about: "a daily problem solver in <b>HackerRank</b>  and <b>Leet Code</b> ",
  // },
  {
    Icon: AiOutlineAntDesign,
    title: "UI/UX designer",
    about:
      "stunning user interface designer using <b>Figma</b>  ",
  },
  // {
  //   Icon: RiComputerLine,
  //   title: "Whatever",
  //   about:
  //     "Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic quis minima autem!",
  // },
];

export const languages: Skill[] = [
  {
    Icon: BsCircleFill,
    name: "Python",
    level: "50",
  },
  {
    Icon: BsCircleFill,
    name: "Java Script",
    level: "80",
  },
  {
    Icon: BsCircleFill,
    name: "ASP.NET C#",
    level: "50",
  },
  {
    Icon: BsCircleFill,
    name: "React.js",
    level: "70",
  },
  {
    Icon: BsCircleFill,
    name: "Angular",
    level: "50",
  },
  {
    Icon: BsCircleFill,
    name: "Next.js",
    level: "80",
  },
  {
    Icon: BsCircleFill,
    name: "Bootstrap",
    level: "60",
  },
  {
    Icon: BsCircleFill,
    name: "Tailwind",
    level: "80",
  },
];

export const tools: Skill[] = [
  {
    Icon: BsCircleFill,
    name: "Postman",
    level: "80",
  }, 
  {
    Icon: BsCircleFill,
    name: "GitHub",
    level: "80",
  }, 
  {
    Icon: BsCircleFill,
    name: "GitLab",
    level: "80",
  }, 
  {
    Icon: BsCircleFill,
    name: "Figma",
    level: "65",
  },
  {
    Icon: BsCircleFill,
    name: "Photoshop",
    level: "80",
  },
  {
    Icon: BsCircleFill,
    name: "Illustrator",
    level: "65",
  },
];

export const projects: IProject[] = [
  {
    name: "Bestcare",
    description:
      "This website for recruiting and hiring housewives.",
    image_path: "/images/bestcare/Screenshot 2022-10-10 105137.jpg",
    deployed_url: "https://bestcareprofessional.com",
    github_url: "",
    category: ["Next.js"],
    key_techs: ["Next.js", "Tailwind", "Node"],
  },
  {
    name: "Coincap",
    image_path: "/images/coincap/Screenshot 2022-10-10 104843.jpg",
    deployed_url: "https://coincap-blackpoi55.vercel.app",
    github_url: "",
    category: ["Next.js"],
    description:
      "A website for viewing the prices of various coins in the crypto market. And calculate the payback rate of Bomb Crypto NFT without data storage in the database. Instead, it stores data in local storage for user peace of mind.",
    key_techs: ["Next.js", "Tailwind"],
  },
  {
    name: "Sky-Line",
    image_path: "/images/skyline/Screenshot 2022-10-10 161459.jpg",
    deployed_url: "",
    github_url: "",
    category: ["Next.js"],
    description:
      "Website for steel trading",
    key_techs: ["Next.js", "Tailwind"],
  },
  {
    name: "Nursing Home",
    image_path: "/images/nursinghome/Screenshot 2022-10-10 152528.jpg",
    deployed_url: "",
    github_url: "",
    category: ["Next.js"],
    description:
      "Website for collecting information on patients who have a doctor to take care of the elderly at home.",
    key_techs: ["Next.js", "BootStap", "React-Chartjs-2"],
  },
  {
    name: "Smart School",
    image_path: "/images/smartschool/930582128722133.jpg",
    deployed_url: "",
    github_url: "",
    category: ["Next.js"],
    description:
      "Website for managing the check-in system, managing information, teachers, students of the school..",
    key_techs: ["Next.js", "Tailwind", "React-Chartjs-2"],
  },
  {
    name: "Elevator-Booking",
    image_path: "/images/elevator-booking/1665388553472.jpg",
    deployed_url: "",
    github_url: "",
    category: ["react.js"],
    description:
      "Website for booking elevators. Rajavithi Hospital.",
    key_techs: ["react.js", "Big Calenda", "BootStap"],
  },
  {
    name: "Bar Code & QR Code",
    image_path: "/images/barcodegen/Screenshot 2022-10-10 160436.jpg",
    deployed_url: "",
    github_url: "",
    category: ["Next.js"],
    description:
      "Website for managing the check-in system, managing information, teachers, students of the school.",
    key_techs: ["Next.js", "Tailwind", "react-qr-code"],
  },
  {
    name: "RSU",
    image_path: "/images/rsu/Screenshot 2022-10-10 170726.jpg",
    deployed_url: "",
    github_url: "",
    category: ["Next.js"],
    description:
      "Website for student assessment of Rangsit University",
    key_techs: ["Next.js", "Tailwind"],
  },
  {
    name: "Video Capture",
    image_path: "/images/videocapture/Screenshot 2022-10-10 104218.jpg",
    deployed_url: "",
    github_url: "",
    category: ["C#"],
    description:
      "Program for capturing images and videos for connecting medical devices.",
    key_techs: ["C#", "Visio Force"],
  },
  {
    name: "Contributordr (แพทย์พี่เลี้ยง)",
    image_path: "/images/contributordr/Screenshot 2022-10-10 132357.jpg",
    deployed_url: "https://icpird.moph.go.th/contributordr/",
    github_url: "",
    category: ["ASP.NET"],
    description:
      "Website for recruiting medical consultants Ministry of Health.",
    key_techs: ["ASP.NET"],
  },
  {
    name: "e-Budgeting (จัดการงบประมาณ)",
    image_path: "/images/e-Budgeting/Screenshot 2022-10-10 133146.jpg",
    deployed_url: "",
    github_url: "",
    category: ["ASP.NET"],
    description:
      "Website for manage budget Ministry of Health.",
    key_techs: ["ASP.NET"],
  },
  {
    name: "ICpirdStudent (จัดการทุนนักศึกษาแพทย์)",
    image_path: "/images/icpirdstudent/1665383261282.jpg",
    deployed_url: "",
    github_url: "",
    category: ["ASP.NET"],
    description:
      "Website for Manage scholarships for medical students, Ministry of Public Health.",
    key_techs: ["ASP.NET"],
  },
  {
    name: "Resident (แพทย์ประจำบ้าน)",
    image_path: "/images/resident/Screenshot 2022-10-10 133258.jpg",
    deployed_url: "https://icpird.moph.go.th/resident/",
    github_url: "",
    category: ["ASP.NET"],
    description:
      "Website for applying for a home doctor Ministry of Health.",
    key_techs: ["ASP.NET"],
  },
  {
    name: "Inventory (คลังยา)",
    image_path: "/images/inventory/Screenshot 2022-10-10 151119.jpg",
    deployed_url: "",
    github_url: "",
    category: ["Angular"],
    description:
      "Website for picking up medicines, drug stocks, Rajavithi Hospital",
    key_techs: ["Angular", "BootStap"],
  },
];
